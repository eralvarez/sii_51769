// Mundo.h: interface for the CMundo class.
//Este código pertenece a Elena Rodriguez
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <sys/types.h>
#include <sys/stat.h>


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "DatosMemCompartida.h"
#define MAX_CAD 200

class CMundoCliente  
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
//	int pdatos;
	char cad[MAX_CAD];
	char cad_teclas[MAX_CAD];
	int fd;
	char *myfifo;
	int fd_coordenadas;
	int fd_teclas;
	int fd_datos;
	DatosMemCompartida datos;
	DatosMemCompartida *pdatos;
	char *org;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
