#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>
int main(){

//Creación de variables.
	DatosMemCompartida *pdatos;
	int fd;
	char* org;
	float mitadRaqueta;

//Abrir fichero
	fd=open("DatosMemComp",O_RDWR|O_CREAT,0777);
	if(fd==-1){
		perror("Error al abrir el fichero de la memoria compartida\n");
		exit(1);
	}

//Proyecta en memoria un fichero
//La estructura es void *mmap(void *addr, size_t length, int prot, int flags,int fd, off_t offset);
	org=(char*)mmap(NULL, sizeof(*(pdatos)), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
//Retorna un puntero a la zona mapeada, si existe un error retorna -1
	if (org==MAP_FAILED)
	{
		perror("Error en la proyeccion del fichero origen");
		close(fd);
		exit(1);
	}	

//Cerrar el descriptor de fichero.
	close(fd);

//Asignar la dirección de comienzo de la región creada al atributo de tipo puntero. Hacemos un cast para convertirlo al mismo tipo.
	pdatos=(DatosMemCompartida*)org;

	//bucle infinito
	while(1){
		if(pdatos->accion==3)break;
//Salimos del bucle, el juego ha terminado.

		mitadRaqueta=((pdatos->raqueta1.y1+pdatos->raqueta1.y2)/2);
		if(mitadRaqueta < (pdatos->esfera.centro.y))
//Si la mitad de la raqueta se encuentra por debajo del el centro de la esfera
			pdatos->accion=1; //Arriba

		else if(mitadRaqueta > (pdatos->esfera.centro.y))
//Si la mitad de la raqueta se encuentra por encima del centro de la esfera
			pdatos->accion=-1; //Abajo
		
		else pdatos->accion=0; //Nada, coincide, están alineadas.
		
//Para que el tiempo de respuesta no sea inmediato.
		usleep(2500);
	}

//Elimina la proyección de memoria de un fichero.
//La estrucutura es int munmap(void *addr, size_t length);
//Cuando tiene éxito retorna 0 y cuando hay un error -1
	if(munmap(org,sizeof(*(pdatos)))==-1){
		perror("Error al eliminar la proyección de memoria compartida\n");
		exit(1);
	}	
  }
