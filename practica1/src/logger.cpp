#define TAM_CAD 200
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>


int main (void){
	
//Declaramos las variables
	unlink("myfifo");
	char buffer[TAM_CAD];
	int fd;int flag=1;


//Mensaje de error si no podemos crear el fifo.
	if(mkfifo("myfifo",0777)==-1){
		perror("Error al crear el fifo");
		return 0;
	}

//Abrimos el fifo, si no se puede devuelve un mensaje de error.
	fd=open("myfifo",O_RDONLY);
	if(fd==-1) {
		perror("Error al abrir el fifo");
		unlink ("myfifo");
		return 0;
	}
	printf("Iniciamos el juego \n");

//Bucle de lectura, lo ejecuta hasta que exista un carácter especial.
	while(flag==1){
	if(read(fd,buffer,TAM_CAD)==-1){
		perror("Error al leer de la tubería");
		break;
	}
	printf("%s",buffer);
	if(buffer[0]=='#')flag=0;
	}

	close(fd);
	unlink("myfifo");
	return 0;	
}
